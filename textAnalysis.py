from collections import Set
import regex as re
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from typing import List
import math
import numpy as np


class Sentence:
    def __init__(self, stemmed_words, original_words, document_index):
        self._stemmed_words = stemmed_words
        self._original_words = original_words
        self._document_index = document_index
        self._lex_rank_score = None
        self._word_frequencies = self.calculate_words_frequencies()

    @property
    def stemmed_words(self):
        return self._stemmed_words

    @property
    def original_words(self):
        return self._original_words

    @property
    def lex_rank_score(self):
        return self._lex_rank_score

    @property
    def word_frequencies(self):
        return self._word_frequencies

    @property
    def document_index(self):
        return self._document_index

    @lex_rank_score.setter
    def lex_rank_score(self, score):
        self._lex_rank_score = score

    def calculate_words_frequencies(self) -> dict:
        word_frequencies = {}
        for word in self._stemmed_words:
            if word in word_frequencies:
                word_frequencies[word] += 1
            else:
                word_frequencies[word] = 1

        return word_frequencies


class PreProcessing:
    def __init__(self):
        self.stop_words = stopwords.words('french')
        self.stop_words.append('a')
        self.stop_words.append('b')

    def remove_stopwords(self, sentence) -> List[str]:
        return [word for word in sentence if word not in self.stop_words]

    def process_text(self, text_string: str, document_index: int) -> List[Sentence]:
        stemmer = SnowballStemmer('french')

        lines = nltk.tokenize.sent_tokenize(text_string, language='french')
        sentences = []
        for sentence in lines:
            tokenized_sentence = sentence.strip().lower()
            tokenized_sentence = nltk.word_tokenize(tokenized_sentence, language='french')
            tokenized_sentence = self.remove_stopwords(tokenized_sentence)

            stemmed_sentence = [stemmer.stem(word) for word in tokenized_sentence]
            stemmed_sentence = list(filter(lambda w: w != '?' \
                                                     and w != ',' \
                                                     and w != '!' \
                                                     and w != '.' \
                                                     and w != ':' \
                                                     and w != '-' \
                                                     and w != '(' \
                                                     and w != ')' \
                                                     and w != ';' \
                                                     and w != '[' \
                                                     and w != ']' \
                                                     and w != '{' \
                                                     and w != '}' \
                                                     and w != '/' \
                                                     and w != 'http' \
                                                     and w != '«' \
                                                     and w != '»' \
                                                     and w != 'https' \
                                                     and not w.startswith('//') \
                                                     and w != '...', stemmed_sentence))
            if stemmed_sentence:
                sentences.append(Sentence(stemmed_sentence, sentence, document_index))

        return sentences


class ElasticsearchProcessing:
    def sanitize_text(self, text: str) -> str:
        cleaned = text.strip().replace('\n', ' ')
        return re.sub(r'\s{2,}', ' ', cleaned)

    def _extract_sentences_with_keywords(self, keywords: List[str], text: str):
        regex_str = r'([^.]*?{}[^.]*\.)'
        sentences = []
        for keyword in keywords:
            sentences.extend(re.findall(regex_str.format(keyword), text))

        return sentences

    def extracts_sentences_with_keywords_in_result(self, keywords, result):
        sentences = []
        for r in result['hits']['hits']:
            text = self.sanitize_text(r['_source']['content'])
            sentences.extend(self._extract_sentences_with_keywords(keywords, text))

        return sentences

    def extract_texts_from_elasticsearch_result(self, result) -> List[str]:
        return list(map(lambda r: self.sanitize_text(r['_source']['content']), result['hits']['hits']))


class LexRank:
    def __init__(self, documents: List[str], threshold: float, epsilon: float):
        self._documents = documents
        self.text_processor = PreProcessing()
        self.sentences = []
        self._threshold = threshold
        self._epsilon = epsilon

        print('Processing documents...')
        for index, doc in enumerate(self._documents):
            self.sentences.extend(self.text_processor.process_text(doc, index))
        print('Done')

        print('calculating idfs...')
        self.idfs = self._calculate_idfs()
        print('Done')
        print(self.idfs)

    def generate_summary(self, size) -> List[str]:
        sentences_count = len(self.sentences)
        if sentences_count == 0:
            return
        matrix = np.zeros((sentences_count, sentences_count))

        print('Generating scores...')
        self.score(matrix)
        print('Done')

        summary = sorted(self.sentences, key=lambda x: x.lex_rank_score, reverse=True)[:size]
        sorted_summary = list(filter(lambda x: x in summary, self.sentences))
        sorted_summary = map(lambda x: x.original_words, sorted_summary)

        return list(sorted_summary)

    def tfw(self, word: str, sentence: Sentence) -> int:
        return sentence.word_frequencies.get(word, 0)

    def score(self, matrix: np.ndarray):
        sentences_count = len(self.sentences)
        degrees = np.zeros((sentences_count, ))

        for row in range(sentences_count):
            print(f'{row}/{sentences_count}')
            for col in range(sentences_count):
                matrix[row, col] = self._cosine_similarity(self.sentences[row], self.sentences[col])

                if matrix[row, col] > self._threshold:
                    matrix[row, col] = 1.0
                    degrees[row] += 1
                else:
                    matrix[row, col] = 0

        for row in range(sentences_count):
            for col in range(sentences_count):
                if degrees[row] == 0:
                    degrees[row] = 1

                matrix[row, col] = matrix[row, col] / degrees[row]

        normalized_score = self.normalize(self.powerMethod(matrix))

        for idx, score in enumerate(normalized_score):
            self.sentences[idx].lex_rank_score = score

    def powerMethod(self, matrix: np.ndarray):
        sentences_count = len(self.sentences)
        transposed_matrix = matrix.transpose()
        p_vector = np.array([1.0 / sentences_count] * sentences_count)
        lambda_val = 1.0

        while lambda_val > self._epsilon:
            next_p = np.dot(transposed_matrix, p_vector)
            lambda_val = np.linalg.norm(np.subtract(next_p, p_vector))
            p_vector = next_p

        return list(p_vector)

    def normalize(self, scores):
        max_score = max(scores)
        normalized_score = []

        for score in scores:
            normalized_score.append(score / max_score)

        return normalized_score

    # For this algorithm, each sentence is considered a document
    def _calculate_idfs(self) -> dict:
        words = {}
        idfs = {}
        N = float(len(self.sentences))

        for sentence in self.sentences:
            for word in frozenset(sentence.stemmed_words):
                words[word] = words.get(word, 0) + 1

        for word in words:
            n = float(words[word])
            idfs[word] = math.log10(N/n)

        return idfs

    def _cosine_similarity(self, sentence1: Sentence, sentence2: Sentence):
        unique_words1 = frozenset(sentence1.stemmed_words)
        unique_words2 = frozenset(sentence2.stemmed_words)

        common_words = unique_words1 & unique_words2
        numerator = 0.0

        for term in common_words:
            numerator += self.tfw(term, sentence1) * self.tfw(term, sentence2) * (self.idfs[term]**2)

        denominator1 = sum((self.tfw(t, sentence1) * self.idfs[t]) ** 2 for t in unique_words1)
        denominator2 = sum((self.tfw(t, sentence2) * self.idfs[t]) ** 2 for t in unique_words2)

        if denominator1 > 0 and denominator2 > 0:
            return numerator / (math.sqrt(denominator1) * math.sqrt(denominator2))
        else:
            return 0.0

