from helpers import get_env
import nltk
from app import App

host = get_env('RABBITMQ_URL')
queue = get_env('RABBITMQ_QUEUE')
username = get_env('RABBITMQ_USER')
password = get_env('RABBITMQ_PASS')

api_url = get_env('API_URL')
api_username = get_env('API_USERNAME')
api_password = get_env('API_PASSWORD')

elasticsearch_host = get_env('ELASTICSEARCH_URL')

nltk_download_dir = get_env('NLTK_DATA')

nltk.download('punkt', download_dir=nltk_download_dir)
nltk.download('stopwords', download_dir=nltk_download_dir)

app = App(host, queue, username, password, api_url, api_username, api_password, elasticsearch_host)
app.rabbitmq_listening()
