import os


def get_env(name: str) -> str:
    if name not in os.environ:
        raise Exception(f"Please specify the '{name}' environment variable.")

    return os.getenv(name)
