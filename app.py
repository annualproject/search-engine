import pika
from api import ApiConnection
from elasticsearchClient import ElasticSearchClient
from textAnalysis import ElasticsearchProcessing, LexRank, PreProcessing
import json
import logging


class App:
    def __init__(self, host, queue, username, password, api_url, api_username, api_password, es_url):
        logging.basicConfig(filename='error.log', level=logging.ERROR)
        self.api_client = ApiConnection(api_url, api_username, api_password)
        self.es_connection = ElasticSearchClient(es_url)
        self.es_processor = ElasticsearchProcessing()
        self.preprocessor = PreProcessing()
        self._host = host
        self._username = username
        self._password = password
        self._queue = queue

    def _handle_message(self, ch, method, properties, body):
        message = json.loads(body.decode('utf-8'))
        task_uuid = message.get('taskUuid')

        try:
            task = self.api_client.get_analyse_task(task_uuid)
        except Exception:
            logging.error(f'Error while retrieving the task of uuid : {task_uuid}')
            return

        try:
            document_set = self.api_client.get_document_set(task.get('documentSetUuid'))
        except Exception:
            logging.error(f'Error while retrieving document set')
            return

        size = task.get('summarySize')
        query = task.get('queryString')
        query = self.preprocessor.remove_stopwords(self.es_processor.sanitize_text(query).split(' '))

        result = self.es_connection.query(' '.join(query), document_set.get('name'))

        if task.get('documentSummary'):
            documents = self.es_processor.extract_texts_from_elasticsearch_result(result)
        else:
            documents = self.es_processor.extracts_sentences_with_keywords_in_result(query, result)

        try:
            self.api_client.update_task_analyse_status(task_uuid, 1)
            lexrank = LexRank(documents, 0.1, 0.1)

            summary = lexrank.generate_summary(size)

            if summary != None:
                summary_text = '\n'.join(summary)
            else:
                summary_text = ''

            self.api_client.update_task_analysys_summary(task_uuid, summary_text)
            self.api_client.update_task_analyse_status(task_uuid, 2)

        except Exception as e:
            self.api_client.update_task_analyse_status(task_uuid, 3)  # 3 stands for the error status in the API
            logging.error(f'Error during lexRank algorithm : {str(e)}')

        self._ack_message(ch, method.delivery_tag)

    def _generate_document_summary(self, index_name, keywords, summary_size):
        query = ' '.join(keywords)
        documents = self.es_connection.query(query, index_name)
        documents = self.es_processor.extract_texts_from_elasticsearch_result(documents)

        lexrank = LexRank(documents, 0.1, 0.1)
        summary = lexrank.generate_summary(summary_size)

        return summary

    def _generate_sentences_summary(self, index_name, keywords, summary_size):
        query = ' '.join(keywords)
        documents = self.es_connection.query(query, index_name)
        sentences = self.es_processor.extracts_sentences_with_keywords_in_result(keywords, documents)

        lexrank = LexRank(sentences, 0.1, 0.1)
        summary = lexrank.generate_summary(summary_size)

        return summary

    def _ack_message(self, channel, delivery_tag):
        channel.basic_ack(delivery_tag)

    def rabbitmq_listening(self):
        credentials = pika.PlainCredentials(self._username, self._password)

        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self._host, credentials=credentials)
        )

        channel = connection.channel()
        channel.queue_declare(queue=self._queue)

        channel.basic_consume(
            queue=self._queue, on_message_callback=self._handle_message
        )

        print('Listening for messages. To exit, press CTRL+C')

        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()

        connection.close()
