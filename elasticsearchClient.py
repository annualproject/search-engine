from elasticsearch import Elasticsearch


class ElasticSearchClient:
    _es = None

    def __init__(self, url):
        self._es = Elasticsearch(hosts=url)
        print(self._es.info())

    def query(self, query, index_name):
        query_string = {
            'query': {
                'match': {
                    'content': {
                        'query': query
                    }
                }
            }
        }

        return self._es.search(index=index_name, body=query_string)
