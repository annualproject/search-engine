FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ARG REQUIREMENTS
COPY requirements/ /code/
RUN pip install -r ${REQUIREMENTS}
COPY . /code/
