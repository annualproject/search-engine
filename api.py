import requests


class ApiConnection:
    AUTH_URL = 'auth'
    GET_DOCUMENTS_URL = 'documentSets/{uuid}/documents'
    GET_TASK_URL = 'taskAnalyse/{uuid}'
    UPDATE_TASK_STATUS_URL = 'taskAnalyse/{uuid}/status'
    UPDATE_TASK_SUMMARY_URL = 'taskAnalyse/{uuid}/synthesis'
    GET_DOCUMENT_SET_URL = 'documentSets/{uuid}'

    def __init__(self, url, username, password):
        self._url = url
        self._token = None
        self.auth(username, password)

    def auth(self, username, password):
        payload = {
            'username': username,
            'password': password
        }

        r = requests.post(f'{self._url}{self.AUTH_URL}', json=payload)
        r.raise_for_status()

        if 'jwt' in r.json():
            self._token = r.json()['jwt']

    def _get_auth_headers(self) -> dict:
        return {
            'Authorization': f'Bearer {self._token}'
        }

    def get_documents_from_set(self, set_uuid: str):
        url = self.GET_DOCUMENTS_URL.format(uuid=set_uuid)

        r = requests.get(f'{self._url}{url}', headers=self._get_auth_headers())
        r.raise_for_status()

        return r.json()

    def get_analyse_task(self, task_uuid: str):
        url = self.GET_TASK_URL.format(uuid=task_uuid)

        r = requests.get(f'{self._url}{url}', headers=self._get_auth_headers())
        r.raise_for_status()

        return r.json()

    def update_task_analyse_status(self, task_uuid: str, status: int):
        url = self.UPDATE_TASK_STATUS_URL.format(uuid=task_uuid)
        data = {
            'status': status
        }

        r = requests.put(f'{self._url}{url}', json=data, headers=self._get_auth_headers())
        r.raise_for_status()

    def update_task_analysys_summary(self, task_uuid: str, summary: str):
        url = self.UPDATE_TASK_SUMMARY_URL.format(uuid=task_uuid)
        data = {
            'synthesis': summary
        }

        r = requests.put(f'{self._url}{url}', json=data, headers=self._get_auth_headers())
        r.raise_for_status()

    def get_document_set(self, set_uuid):
        url = self.GET_DOCUMENT_SET_URL.format(uuid=set_uuid)

        r = requests.get(f'{self._url}{url}', headers=self._get_auth_headers())
        r.raise_for_status()

        return r.json()
